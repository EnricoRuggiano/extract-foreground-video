# Extract Foreground Video
Have you ever dreamt to extract people in foreground from a video? Well, with this toolbox we try to experiment some strategies to do so.

Also the output video will have embedded the silhouette mask, so you can easily find out if there are some artefacts in the output results.

## The algorithms available

```python
from src.algorithm      import algorithm
from src.algorithm_base import algorithm_base
from src.algorithm_auto import algorithm_auto
from src.algorithm_auto import algorithm_auto_with_mask
from src.utils          import *
from src.subtract_background import subtract_background
```

We have the following algorithms:
* `algorithm_base.py`: a chroma key algorithm that filter the foreground based on the luma colors
* `algorithm.py`: Calculates a draft mask using `algorithm_base.py` then it creates a mask from the contours of the color mask.
* `algorithm_auto.py`: an `algorithm.py` variant that combines the chroma key with the OpenCv grub_cut
* `subtract_background.py`: if a static image of the background is available you can use it to extract the foreground of the frame using this algorithm.

```
As always, it does not exists a best algorithm. The results depend on the input video and on the parameters used.
```

Tuning the parameters of course it is important to obtain decent results. To do so, we use some programs available in this toolbox. 


## Subtract Background

```python
FRAME_1 = 'res/screenshots/beach.jpg'
BG_1    = 'res/screenshots/beach_bg.png'

frame = cv2.imread(FRAME_1)
png   = cv2.imread(BG_1)


full, mask, fg, bg = subtract_background(frame, png)
```

![preview](doc/output_1.png "1")

## Chroma Key + Contour Detection
Here we use an algorithm that combines the chroma key with contour detection in the mask. This results have been considered our baseline for a lot. 

```python
FRAME_2 = 'res/screenshots/theater.jpg'

frame = cv2.imread(FRAME_2)

# parameters
cfg = {"l_h": 115, "u_h": 250, "l_s": 0, "u_s": 255, "l_v": 0, "u_v": 255, "algo": 0, "inv": 0, "width": 800, "height": 572}
args = cfg.copy()
del args['inv']
del args['algo']
del args['width']
del args['height']

full, mask, fg, bg = algorithm(frame, **args)
```

![preview](doc/output_2.png "2")

## Difference between the Subtrack Background vs Chroma Key + Contour Detection
Sometimes the `algorithm.py` seems really easy and may work pretty well. Sometimes using a static background image may really help to obtain better results.

Let's take a look here:

```python
from src.utils import * 

FRAME_4 = 'res/screenshots/plane.jpg'
BG_4    = 'res/screenshots/plane_bg.jpg'

frame = cv2.imread(FRAME_4)
png   = cv2.imread(BG_4)

_, mask_bg, res_bg, _ = subtract_background(frame, png)

cfg = {"l_h": 29, "u_h": 120, "l_s": 75, "u_s": 255, "l_v": 0, "u_v": 255}
_, ch, res_ch, _ = algorithm(frame, **cfg)
```

![preview](doc/output_3.png "3")
![preview](doc/output_4.png "4")


## Grub cut
Finally we have an algorithm variation of the OpenCv grub_cut. Take care that this algorithm is very powerful, can solve a lot of problems but it is very heavy, specially with video with high resolutions (>= 720p)

```python
from src.utils import * 

FRAME_5 = 'res/screenshots/blue.jpg'

frame = cv2.imread(FRAME_5)

cfg = {"l_h": 118, "u_h": 179, "l_s": 0, "u_s": 255, "l_v": 0, "u_v": 255}

full, mask, fg, bg = algorithm(frame, **cfg)
full, mask, _, bg = algorithm_auto_with_mask(frame, mask)
print("Resolution: {}x{}".format(frame.shape[1], frame.shape[0]))
```

![preview](doc/output_5.png "5")


---
## The Platform

The platform follows this structure.

```

/notebooks
/res
    /cfg
    /input
    /output
    /tmp
    /screenshots
/scripts
/src
/test

```


In this repository will find some python experiments in `/notebooks` with some juicy details of the algorithms. 

In `/scripts` you will find some usefull video, image manipulation very useful for this toolbox (some of them come from my personal ffmpeg scripts library)

In `/src` you have the algorithms code well organized and in `/res` some interesting test asset that I "stole" from the internet.

Finally in `/test` you have a web framework that acts as video gallery, so that you can check mistakes in the video results.

---

## How to install

Here are the main dependency
 * FFmpeg
 * OpenCv

Please take care also to install the python libraries

 ```bash
pip install -r requirements.txt
 ```

---

## How To run

You can run this program in two ways, in `preview mode` and in `render mode`.

## Preview Mode
`python3 preview.py <input_video.mp4> <optional background_img.jpg>` 

Using the preview mode you can view at the same time the foreground extracted from the input video and the masked background.


You use this mode to find the best algorithm configuration.

Commands:
```
-------------
ESC - to exit
s   - to save the config
p   - to save the input frame
d   - force the default configuration (Available only when a background image is provided)
-------------
```

![preview](doc/screen_1.png "Preview mode")

When you pressed `s` to save the configuration file you can proceed to render the output video.

---

## Render Mode
Run this line to execute the export.

`python3 render.py <input_video.mp4> <setup.json> <output_video.mp4>`

This program creates a `tmp.avi` video with the foreground and the mask calculated. 

By default it renders only the first `100 frames` of the video input. 

When it finishes, it creates an `output.mp4` video file with `FFmpeg`

![test](doc/screen_3.png "Rendering")


---

## Testing
Of course sometimes playing the video in `vlc` is not enough and may not be helpful to understand fully if the foreground was extracted in a decent way. So, you can test the output videos in a local framework that renders them in a quick way.

To do so run the follow commands inside the `Makefile`. 
You need `Docker` to start the local framework.

![test](doc/screen_2.png "Testing")

To build the docker image of the local framework:
```
make test-build
```

To start the service:
```
make test-run
```

To stop the service:
```
make test-stop
```



---

# Note


I don't have the credits for the images & videos used in the experiments. I used them just for fun :)!



![note](doc/note-meme.jpg "meme")
