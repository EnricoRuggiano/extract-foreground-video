import cv2
import numpy as np
import sys
import os
import json
import time 
from subprocess import call
from src.algorithm 		import algorithm
from src.algorithm_base import algorithm_base
from src.algorithm_auto import algorithm_auto_with_mask
from src.subtract_background import subtract_background

if len(sys.argv) != 4:
	print("Error: no input argument")
	print("python3 render.py <input_video.mp4> <config.json> <out_video.mp4>")
	sys.exit()

print("-------------")
print("Extracting input video")

cap = cv2.VideoCapture(sys.argv[1])
cfg = json.loads(open(sys.argv[2]).read())

HAS_BACKGROUND = cfg['algo'] == 3 and 'bck' in cfg.keys()

# Define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter("tmp.avi", fourcc, 30.0, (cfg["width"],  cfg["height"] * 2))

def nothing(x):
	pass

num = 0
start_t = time.time()
t = time.time()

while cap.isOpened():
	_, frame = cap.read()

	if HAS_BACKGROUND:
		thr 	= cfg['thr']
		dilate  = cfg['dil']
		erode 	= cfg['er']
		sharp 	= cfg['sh']
		blur 	= cfg['blur']
		png     = png = cv2.imread(cfg['bck'])

	else:
		l_h = cfg["l_h"]
		u_h = cfg["u_h"]
		l_s = cfg["l_h"]
		u_s = cfg["u_s"]
		l_v = cfg["l_v"]
		u_v = cfg["u_v"]
	
	algo = cfg["algo"]
	inv  = cfg["inv"]
    
	num += 1
	t = time.time()

	try:
		if algo == 0:
			full, mask, _, bg = algorithm(frame  \
						, l_h \
						, l_s \
						, l_v \
						, u_h \
						, u_s \
						, u_v )
		if algo == 1:
			full, mask, _, bg = algorithm_base(frame  \
						, l_h \
						, l_s \
						, l_v \
						, u_h \
						, u_s \
						, u_v )
		if algo == 2:
			full, mask, fg, bg = algorithm(frame  \
					, l_h \
					, l_s \
					, l_v \
					, u_h \
					, u_s \
					, u_v )

			full, mask, _, bg = algorithm_auto_with_mask(frame, mask)

		if algo == 3 and HAS_BACKGROUND:
			full, mask, _, _ = subtract_background(frame, png, thr=thr, in_dilate=dilate, in_erode=erode, in_sharp=sharp, in_blur=blur)

		if inv == 0:
			# write	
			out.write(full)
			print("frame number {} processed in {:.2f} minutes".format(num, (time.time() - t)/60))

		if inv == 1:
			mask_inv = cv2.bitwise_not(mask)
			mask_inv_bgr = cv2.cvtColor(mask_inv, cv2.COLOR_GRAY2BGR)
			full = np.vstack((bg, mask_inv_bgr))

			# write	
			out.write(full)
			print("frame number {} processed in {:.2f} minutes".format(num, (time.time() - t)/60))
   
		if num == 100:
			break
	except Exception as e:
		print("exception occured {}".format(e))
		break

cap.release()
out.release()
cv2.destroyAllWindows()

print("-------------")
print("rendered ended in {:.2f} minutes".format((time.time() - start_t)/60))
print("-------------")
print("Converting to .mp4")
command = "bash {} tmp.avi {}".format(os.path.join('scripts', 'to_mp4.sh'), sys.argv[3]) 
call(command.split())

if os.path.exists("tmp.avi") and os.path.exists(sys.argv[3]):
	print("-------------")
	print("Delete tmp.avi file")
	os.remove("tmp.avi")
