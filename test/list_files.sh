#!/bin/sh

if [ -f filename.js ]
then
rm filename.js
fi

touch filename.js
echo "var index = 0" >> filename.js
echo "var filenames=[" >> filename.js

i=0
for file in $1/*.mp4
do
  if [ $i -gt 0 ]
  then
   echo ", " >> filename.js
  fi
  name=$(basename $file)
  echo -n " 'res/$name' " >> filename.js
  i=$(($i + 1))
done

echo "	];" >> filename.js
