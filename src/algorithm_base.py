import cv2
import numpy as np

def algorithm_base( frame  \
				, l_h \
				, l_s \
				, l_v \
				, u_h \
				, u_s \
				, u_v ):
	
    # convert input frame to hsv image
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
	
    # apply blur
    hsv = cv2.GaussianBlur(hsv, (5, 5), 0)

    # init thresholds
    lower_green = np.array([l_h, l_s, l_v])
    upper_green = np.array([u_h, u_s, u_v])
    
    # color mask
    mask_color = cv2.inRange(hsv, lower_green, upper_green)
    mask_color_inv = cv2.bitwise_not(mask_color)

    # binary draft mask   
    fg_draft = cv2.bitwise_and(frame, frame, mask=mask_color_inv)
    gray_draft = cv2.cvtColor(fg_draft, cv2.COLOR_BGR2GRAY)	 	
    _, mask = cv2.threshold(gray_draft, 1, 255, cv2.THRESH_BINARY) 	

    # apply some blur
    #mask = cv2.blur(mask, (3,3))

    # bitwise and
    mask_inv = cv2.bitwise_not(mask)
    mask_bgr = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)    

    fg = cv2.bitwise_and(frame, frame, mask=mask)
    bg = cv2.bitwise_and(frame, frame, mask=mask_inv)
    
    # stack the frame and the mask
    full = np.vstack((fg, mask_bgr))
 	
    return (full, mask, fg, bg)