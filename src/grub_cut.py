import cv2
import numpy as np
import time 

def run_grubcut_rect(img, mask, rect, iterCount=10):
    fgModel = np.zeros((1, 65), dtype="float")
    bgModel = np.zeros((1, 65), dtype="float")
    
    start = time.time()
    (mask, bgModel, fgModel) = cv2.grabCut(img, mask, rect, bgModel, fgModel, iterCount=iterCount, mode=cv2.GC_INIT_WITH_RECT)
    end = time.time()
    print("[INFO] applying GrabCut took {:.2f} seconds".format(end - start))
    return (mask, bgModel, fgModel)

def run_grubcut_mask(img, mask, rect=None, iterCount=10):
    fgModel = np.zeros((1, 65), dtype="float")
    bgModel = np.zeros((1, 65), dtype="float")
    
    mask[mask > 0] = cv2.GC_PR_FGD
    mask[mask == 0] = cv2.GC_BGD
    
    start = time.time()
    (mask, bgModel, fgModel) = cv2.grabCut(img, mask, rect, bgModel, fgModel, iterCount=iterCount, mode=cv2.GC_INIT_WITH_MASK)
    end = time.time()
    print("[INFO] applying GrabCut took {:.2f} seconds".format(end - start))
    return (mask, bgModel, fgModel)

def process_background(msk):
    return (msk==cv2.GC_PR_BGD).astype("uint8") * 255 + (msk == cv2.GC_PR_FGD).astype("uint8") * 255 + (msk == cv2.GC_FGD).astype("uint8")*255

def process_mask(msk):
    return (msk == cv2.GC_PR_FGD).astype("uint8") * 255 + (msk == cv2.GC_FGD).astype("uint8")*255

def extract_fg(img, msk):
    return cv2.bitwise_and(img, img, mask=msk)

def extract_bg(img, msk):
    return cv2.bitwise_and(img, img, mask=cv2.bitwise_not(msk))

def empty_mask(img):
    return np.zeros(img.shape[:2], dtype="uint8")

def process_contour(contour, img):
    # approximate contours to points
    approx_contours	= cv2.approxPolyDP(contour, 1, False)

    # fill the approximate contours 
    black_image = np.zeros(shape=[img.shape[0],img.shape[1], 3], dtype = "uint8")
    mask_bgr = cv2.fillPoly(black_image, pts =[approx_contours], color=(255,255,255))

	# convert mask to binary
    gray = cv2.cvtColor(mask_bgr, cv2.COLOR_BGR2GRAY)
    _ , mask = cv2.threshold(gray, 1, 255, cv2.THRESH_BINARY)
    return mask


def grub_cut( frame ):
	
    rect = (130, 0, 200, 700)
    out_frame = frame.copy()

    frame = cv2.GaussianBlur(frame, (5, 5), 0, 0)

    (mask, _, _) = run_grubcut_rect(frame, empty_mask(frame), rect, 10)
    mask = cv2.dilate(mask, (12, 12), iterations=1)

    # debug
    test_fg = extract_fg(frame, process_mask(mask)) 
    test_fg = cv2.GaussianBlur(test_fg, (3, 3), 0, 0)

    # get edges
    edges = cv2.Canny(frame, 1, 100)
    _ , edge_mask = cv2.threshold(edges, 1, 255, cv2.THRESH_BINARY)

    # get background mask
    rect_mask = process_background(mask)
    mask_bg = cv2.bitwise_and(rect_mask, rect_mask, mask=edges)
    mask_bg = cv2.GaussianBlur(mask_bg, (3, 3), 0, 0)
    mask_bg = cv2.dilate(mask_bg, (12, 12), iterations=3)

    # union with background mask
    mask = process_mask(mask)
    mask = cv2.bitwise_or(mask, mask_bg)

    # get contours
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_TC89_KCOS)
    best_contour = max(contours, key=cv2.contourArea)
    mask = process_contour(best_contour, frame)
    mask_bgr = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)

    # bitwise and
    mask_inv = cv2.bitwise_not(mask)
    
    fg = cv2.bitwise_and(out_frame, out_frame, mask=mask)
    bg = cv2.bitwise_and(out_frame, out_frame, mask=mask_inv)
    
    # stack the frame and the mask
    full = np.vstack((fg, mask_bgr))
 	
    return (full, mask, fg, bg)


def grub_cut_easy( frame ):
	
    rect = (150, 0, 200, 700)
    out_frame = frame.copy()

    frame = cv2.GaussianBlur(frame, (5, 5), 0, 0)

    (mask, _, _) = run_grubcut_rect(frame, empty_mask(frame), rect, 10)

    # debug
    mask = process_mask(mask)
    mask_bgr = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)

    # bitwise and
    mask_inv = cv2.bitwise_not(mask)
    
    fg = cv2.bitwise_and(out_frame, out_frame, mask=mask)
    bg = cv2.bitwise_and(out_frame, out_frame, mask=mask_inv)
    
    # stack the frame and the mask
    full = np.vstack((fg, mask_bgr))
 	
    return (full, mask, fg, bg)