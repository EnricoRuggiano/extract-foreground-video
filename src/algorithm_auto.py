### import opencv
import cv2 as cv2
import numpy as np
import json  as json
import imutils
from matplotlib.pyplot import imshow
import time

hog = cv2.HOGDescriptor()
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

def run_grubcut_rect(img, mask, rect, iterCount=10):
    fgModel = np.zeros((1, 65), dtype="float")
    bgModel = np.zeros((1, 65), dtype="float")
    
    start = time.time()
    (mask, bgModel, fgModel) = cv2.grabCut(img, mask, rect, bgModel, fgModel, iterCount=iterCount, mode=cv2.GC_INIT_WITH_RECT)
    end = time.time()
    print("[INFO] applying GrabCut took {:.2f} seconds".format(end - start))
    return (mask, bgModel, fgModel)

def run_grubcut_mask(img, mask, rect=None, iterCount=10):
    fgModel = np.zeros((1, 65), dtype="float")
    bgModel = np.zeros((1, 65), dtype="float")
    
    mask[mask > 0] = cv2.GC_PR_FGD
    mask[mask == 0] = cv2.GC_BGD
    
    start = time.time()
    (mask, bgModel, fgModel) = cv2.grabCut(img, mask, rect, bgModel, fgModel, iterCount=iterCount, mode=cv2.GC_INIT_WITH_MASK)
    end = time.time()
    print("[INFO] applying GrabCut took {:.2f} seconds".format(end - start))
    return (mask, bgModel, fgModel)
  
def empty_mask(img):
    return np.zeros(img.shape[:2], dtype="uint8")

def process_mask(msk):
    return (msk == cv2.GC_PR_FGD).astype("uint8") * 255 + (msk == cv2.GC_FGD).astype("uint8") * 255

def extract_fg(img, msk):
    return cv2.bitwise_and(img, img, mask=msk)

def detect_people(img):
    test = imutils.resize(img, width= min(400, img.shape[1]))
    (rects, weights) = hog.detectMultiScale(test, winStride=(4, 4), padding=(4, 4), scale=1.05)
        
    rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
    areas = np.apply_along_axis(lambda r: np.abs(r[0] - r[2])*np.abs(r[1] - r[3]), 1, rects)
    pick = rects[np.argmax(areas)]
    pick = np.expand_dims(pick, axis=0)    
    
    # Coordinate conversion
    x_perc = img.shape[0] / test.shape[0]
    y_perc = img.shape[1] / test.shape[1]    
    pick = np.apply_along_axis(lambda r: np.array([int(r[0]*x_perc), int(r[1]*y_perc), int(r[2]*x_perc), int(r[3]*y_perc)]), 1, pick)    

    return pick

def algorithm_auto(frame, iterCount=10):
    rect = detect_people(frame)
    rect = rect[0]

    (mask, _, _) = run_grubcut_rect(frame, empty_mask(frame), rect, iterCount)
    mask = process_mask(mask)
    
    # bitwise and
    mask_inv = cv2.bitwise_not(mask)
    
    fg = cv2.bitwise_and(frame, frame, mask=mask)
    bg = cv2.bitwise_and(frame, frame, mask=mask_inv)
    
    # stack the frame and the mask
    mask_bgr = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
    full = np.vstack((fg, mask_bgr))

    return (full, mask, fg, bg)
    
def algorithm_auto_with_mask(frame, mask, iterCount=10):
#    rect = detect_people(frame)
#    rect = rect[0]

    (mask, _, _) = run_grubcut_mask(frame, mask, None, iterCount)
    mask = process_mask(mask)
    
    # bitwise and
    mask_inv = cv2.bitwise_not(mask)
    
    fg = cv2.bitwise_and(frame, frame, mask=mask)
    bg = cv2.bitwise_and(frame, frame, mask=mask_inv)
    
    # stack the frame and the mask
    mask_bgr = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
    full = np.vstack((fg, mask_bgr))

    return (full, mask, fg, bg)
