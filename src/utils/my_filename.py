import os

def my_filename(path:str = 'file') -> str: 
    split = path.split(os.path.sep)
    split.reverse()
    filename = split[0]
    name = filename.split('.')
    if len(name) < 1:
        print ("Something wrong in input file")
        return 'file'
    return name[0]