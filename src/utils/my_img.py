import cv2
from matplotlib import pyplot as plt

### utils
def show_cv2(img, title='01'):
    cv2.imshow(title, img)
    cv2.waitKey()
    cv2.destroyAllWindows()

def show(img, title='01', figsize=(20, 20)):
    plt.figure(figsize=figsize)
    if len(img.shape) == 3:
        plt.imshow(img[:,:,::-1])
    elif len(img.shape) == 2:
        plt.imshow(img[:,:], cmap='gray')
    plt.title(title)