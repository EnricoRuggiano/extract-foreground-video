import os

def my_dir(path = 'tmp', parent = None):
	if parent is not None:
		parent_dir = 'res'
		if not os.path.isdir(parent_dir):
			os.makedirs(parent_dir)

		path = os.path.join(parent_dir, path)

	if not os.path.isdir(path):
		os.makedirs(path)
		print('created {} dir'.format("/" + path))
	return path