### import libraries
import os
import cv2 as cv2
import numpy as np
import json  as json
import imutils
import math
from matplotlib import pyplot as plt

def difference(img_1, img_2):
    dst = cv2.absdiff(img_1, img_2)
    return dst

def denoise(image):
    dst = cv2.fastNlMeansDenoising(image)
    return dst

def threshold(image, thr=5):
    _, dst = cv2.threshold(image, thr, 255, cv2.THRESH_BINARY)
    return dst

def blur_1(image, i):
    dst = cv2.GaussianBlur(image, (i, i), 0)
    return dst

def blur_2(image, i):
    dst = cv2.bilateralFilter(image, i, i*2, i / 2)
    return dst

def blur_3(image, i):
    dst = cv2.blur(image, (i, i))
    return dst

def blur_4(image, i):
    dst = cv2.medianBlur(image, i)
    return dst

def erode(image, i):
    element = cv2.getStructuringElement(cv2.MORPH_CROSS, (2 * i + 1, 2 * i + 1), (i, i))
    dst = cv2.erode(image, element)
    return dst

def dilate(image, i):
    element = cv2.getStructuringElement(cv2.MORPH_CROSS, (2 * i + 1, 2 * i + 1), (i, i))
    dst = cv2.dilate(image, element)
    return dst

def contour(image, original):
    contours, _ = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contour_best = max(contours, key = cv2.contourArea)

    approx_contours	= cv2.approxPolyDP(contour_best, 0.01, False)

    black_image = np.zeros(shape=[original.shape[0],original.shape[1], 3], dtype = "uint8")
    mask_bgr = cv2.fillPoly(black_image, pts =[approx_contours], color=(255,255,255))
    return mask_bgr

def unmask(image, mask):
    gray = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
    _ , mask_ok = cv2.threshold(gray, 1, 255, cv2.THRESH_BINARY)

    # bitwise and
    #mask_inv = cv2.bitwise_not(mask_ok)
    fg = cv2.bitwise_and(image, image, mask=mask_ok)
    return fg

def get_mask(image, mask):
    black_image = np.zeros(shape=[image.shape[0],image.shape[1], 3], dtype = "uint8")
    black_image[mask>0] = (255,255,255)
    return black_image


def unsharp_mask(image, kernel_size=(15, 15), sigma=1.0, amount=1.0, threshold=0):
    """Return a sharpened version of the image, using an unsharp mask."""
    blurred = cv2.GaussianBlur(image, kernel_size, sigma)
    sharpened = float(amount + 1) * image - float(amount) * blurred
    sharpened = np.maximum(sharpened, np.zeros(sharpened.shape))
    sharpened = np.minimum(sharpened, 255 * np.ones(sharpened.shape))
    sharpened = sharpened.round().astype(np.uint8)
    if threshold > 0:
        low_contrast_mask = np.absolute(image - blurred) < threshold
        np.copyto(sharpened, image, where=low_contrast_mask)
    return sharpened

def subtract_background(frame, png, thr=5, in_dilate=5, in_erode=5, in_sharp=1.0, in_blur=15):

    # to greyscale
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    png_gray   = cv2.cvtColor(png, cv2.COLOR_BGR2GRAY)

    # difference
    assert(frame_gray.shape == png_gray.shape)
    #difference = cv2.absdiff(frame_gray, png_gray)

    k = frame_gray
    k = blur_4(frame_gray, 3)
    
    h = png_gray
    h = blur_4(png_gray, 3)

    a = difference(k, h)
    a = unsharp_mask(a, amount=in_sharp)
    a = blur_4(a, in_blur)

    #a = denoise(a)
    a = threshold(a, thr)
    a = dilate(a, in_dilate)
    a = erode(a, in_erode)
    a = contour(a, frame)
    #a = blur_3(a, in_blur)

    mask     = a
    mask_inv = cv2.bitwise_not(mask)

    fg = unmask(frame, mask)
    bg = unmask(frame, mask_inv)
    full = np.vstack((fg, mask))
 	
    return (full, mask, fg, bg)