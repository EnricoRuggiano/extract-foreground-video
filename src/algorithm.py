import cv2
import numpy as np

def algorithm( frame  \
				, l_h \
				, l_s \
				, l_v \
				, u_h \
				, u_s \
				, u_v ):
	
    # convert input frame to hsv image
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
	
    # apply blur
    hsv = cv2.GaussianBlur(hsv, (5, 5), 0)

    # init thresholds
    lower_green = np.array([l_h, l_s, l_v])
    upper_green = np.array([u_h, u_s, u_v])
    
    # color mask
    mask_color = cv2.inRange(hsv, lower_green, upper_green)
    mask_color_inv = cv2.bitwise_not(mask_color)

    # binary draft mask   
    fg_draft = cv2.bitwise_and(frame, frame, mask=mask_color_inv)
    gray_draft = cv2.cvtColor(fg_draft, cv2.COLOR_BGR2GRAY)	 	
    _, mask_draft = cv2.threshold(gray_draft, 1, 255, cv2.THRESH_BINARY) 	

    # apply some erode
    kernel = np.ones((1,1), np.uint8)
    mask_draft = cv2.dilate(mask_draft, kernel, iterations=1)
	
    # apply some blur
    mask_draft = cv2.bilateralFilter(mask_draft, 7, 50, 50)
	
    # contour detection on the mask
    contours, _ = cv2.findContours(mask_draft, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_TC89_KCOS)
    contour_best = max(contours, key = cv2.contourArea)
    
    # approximate contours to points
    approx_contours	= cv2.approxPolyDP(contour_best, 0.01, False)

    # fill the approximate contours 
    black_image = np.zeros(shape=[frame.shape[0],frame.shape[1], 3], dtype = "uint8")
    mask_bgr = cv2.fillPoly(black_image, pts =[approx_contours], color=(255,255,255))

	# convert mask to binary
    gray = cv2.cvtColor(mask_bgr, cv2.COLOR_BGR2GRAY)
    _ , mask = cv2.threshold(gray, 1, 255, cv2.THRESH_BINARY)

    # bitwise and
    mask_inv = cv2.bitwise_not(mask)
    
    fg = cv2.bitwise_and(frame, frame, mask=mask)
    bg = cv2.bitwise_and(frame, frame, mask=mask_inv)
    
    # stack the frame and the mask
    full = np.vstack((fg, mask_bgr))
 	
    return (full, mask, fg, bg)