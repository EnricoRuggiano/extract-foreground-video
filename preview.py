import cv2
import sys
import numpy as np
import os
import json
from src.algorithm 			 import algorithm
from src.algorithm_base 	 import algorithm_base
from src.algorithm_auto 	 import algorithm_auto
from src.algorithm_auto 	 import algorithm_auto_with_mask
from src.utils 				 import *
from src.subtract_background import subtract_background

if len(sys.argv) < 2:
	print("Error: no input argument")
	print("python3 preview.py <input_video.mp4> ")
	sys.exit()

HAS_BACKGROUND = False
if len(sys.argv) == 3:
	print("Substract background activated")
	HAS_BACKGROUND = True

if len(sys.argv) >= 4:
	print("Error: wrong arguments")
	print("python3 preview.py <input_video.mp4> ?<optional_background_img.png>")
	sys.exit()

print("-------------")
print("ESC - to exit")
print("s   - to save the config")
print("p   - to save the input frame")

cap = cv2.VideoCapture(sys.argv[1])
png = None

if HAS_BACKGROUND:
	png = cv2.imread(sys.argv[2])

panel = np.zeros([100, 700], np.uint8)
cv2.namedWindow("panel")

def nothing(x):
	pass

cv2.createTrackbar("L – h", "panel", 0, 179, nothing)
cv2.createTrackbar("U – h", "panel", 179, 179, nothing)

cv2.createTrackbar("L – s", "panel", 0, 255, nothing)
cv2.createTrackbar("U – s", "panel", 255, 255, nothing)

cv2.createTrackbar("L – v", "panel", 0, 255, nothing)
cv2.createTrackbar("U – v", "panel", 255, 255, nothing)

cv2.createTrackbar("algorithm", "panel", 0,  3 if HAS_BACKGROUND else 2, nothing)
cv2.createTrackbar("render_fig", "panel", 0, 1, nothing)

if HAS_BACKGROUND:
	cv2.createTrackbar("threshold", "panel", 0, 255, nothing)
	cv2.createTrackbar("sharp", "panel", 1, 200, nothing)
	cv2.createTrackbar("blur", "panel", 1, 30, nothing)
	cv2.createTrackbar("dilate", "panel", 1, 15, nothing)
	cv2.createTrackbar("erode", "panel", 1, 15, nothing)

while True:
	_, frame = cap.read()
	if frame is None:
		frame = _last_frame
	else:
		_last_frame = frame

	l_h = cv2.getTrackbarPos("L – h", "panel")
	u_h = cv2.getTrackbarPos("U – h", "panel")
	l_s = cv2.getTrackbarPos("L – s", "panel")
	u_s = cv2.getTrackbarPos("U – s", "panel")
	l_v = cv2.getTrackbarPos("L – v", "panel")
	u_v = cv2.getTrackbarPos("U – v", "panel")

	if HAS_BACKGROUND:
		thr 	= cv2.getTrackbarPos('threshold', 'panel')
		dilate  = cv2.getTrackbarPos('dilate', 'panel')
		erode 	= cv2.getTrackbarPos('erode', 'panel')
		sharp 	= cv2.getTrackbarPos('sharp', 'panel')
		blur 	= cv2.getTrackbarPos('blur', 'panel')		

	algo = cv2.getTrackbarPos("algorithm", "panel")
	inv  = cv2.getTrackbarPos("render_fig", "panel")

	try:
		if algo == 0:
			_, _, fg, bg = algorithm(frame  \
					, l_h \
					, l_s \
					, l_v \
					, u_h \
					, u_s \
					, u_v )
		if algo == 1:
			_, _, fg, bg = algorithm_base(frame  \
					, l_h \
					, l_s \
					, l_v \
					, u_h \
					, u_s \
					, u_v )

		if algo == 2:
			full, mask, fg, bg = algorithm(frame  \
					, l_h \
					, l_s \
					, l_v \
					, u_h \
					, u_s \
					, u_v )

			_, _, fg, bg = algorithm_auto_with_mask(frame, mask)
		
		if algo == 3 and HAS_BACKGROUND:
			_, _, fg, bg = subtract_background(frame, png, thr=thr, in_dilate=dilate, in_erode=erode, in_sharp=sharp, in_blur=blur)

		#if HAS_BACKGROUND:
		#	preview_img = np.hstack(())
		if inv == 0:
			preview_img = np.hstack((bg, fg))
		else:
			preview_img = np.hstack((fg, bg))
		cv2.imshow("preview", preview_img)
	except:
		cv2.imshow("preview", frame)	

	cv2.imshow("panel", panel)		
	k = cv2.waitKey(30) & 0xFF
	
	# ESC to quit	
	if k == 27:
		break
	if k == ord('s'):
		cfg_path = os.path.join(my_dir(path='cfg', parent='res'), my_filename(path=sys.argv[1]) + '.json')
		with open(cfg_path , 'w') as f:

			if algo == 3 and HAS_BACKGROUND:
				f.write(json.dumps({"thr": thr, 'dil': dilate, 'er': erode, 'sh': sharp, 'blur': blur, "width": frame.shape[1], "height" : frame.shape[0], "algo": algo, "inv": inv, "bck": sys.argv[2]}))
			else:
				f.write(json.dumps({"l_h": l_h, "u_h": u_h, "l_s": l_s, "u_s": u_s, "l_v": l_v, "u_v": u_v, "algo": algo, "inv": inv, "width": bg.shape[1], "height" : bg.shape[0]}))
		f.close()
		print("SAVED CONFIGURATION!")
	if k == ord('p'):
		print("SAVED SCREENSHOT")
		screenshot_path = os.path.join(my_dir(path='screenshots', parent='res'), my_filename(path=sys.argv[1]) + '.jpg')
		cv2.imwrite(screenshot_path, frame)
	if k == ord('d') and HAS_BACKGROUND:
		print("FORCED DEFAULT PARAMS")
		cv2.setTrackbarPos('threshold', 'panel', 5)
		cv2.setTrackbarPos('dilate', 'panel', 5)
		cv2.setTrackbarPos('erode', 'panel', 5)
		cv2.setTrackbarPos('sharp', 'panel', 1)
		cv2.setTrackbarPos('blur', 'panel', 15)

cap.release()
cv2.destroyAllWindows()