#!/bin/sh
if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ] || [ -z "$4" ]
  then
    echo  "Error"
  	echo  "Usage: ./cut_video.sh <input_video.mp4> <output_video.mp4> <HH:MM:SS> <HH:MM:SS>"
  exit 1
fi

ffmpeg -ss $3 -t $4 -i $1 -c copy  $2
