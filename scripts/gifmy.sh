#!/bin/sh

# default time interval is 10 second
START=0:00;
END=0:10;

# parse -s start time and -t end time
while getopts s:t: option
do
	case "${option}" in
		s) START=${OPTARG};;
		t) END=${OPTARG};;
	esac
done

# check input segmented streaming
if [ -z "$5" ]
  then
    echo "Error. No input video supplied"
  	echo "Usage: ./gifmy -s [start_time] -t [end_time] [video.mp4] [mygif.gif]"
  exit 1
fi

# check output file
if [ -z "$6" ]
  then
    echo "Error. No output file supplied"
  	echo "Usage: ./gifmy -s [start_time] -t [end_time] [video.mp4] [mygif.gif]"
  exit 1
fi

ffmpeg -ss $START -t $END -i $5 -filter_complex "[0:v] fps=12,scale=w=480:h=-1,split [a][b];[a] palettegen=stats_mode=single [p];[b][p] paletteuse=new=1" $6