#!/bin/sh

# default time interval is 10 second
START=0:00;
END=0:10;

NAME=$5;
DIR=$6;
INPUT=$7;

# parse -s start time and -t end time
while getopts s:t: option
do
	case "${option}" in
		s) START=${OPTARG};;
		t) END=${OPTARG};;
	esac
done

# check input segmented streaming
if [ -z "$INPUT" ]
  then
    echo "Error. No input video supplied"
  	echo "Usage: ./framy -s [start_time] -t [end_time] [name] [directory] [video.mp4]"
  exit 1
fi

# check output dir
if [ -z "$DIR" ]
  then
    echo "Error. No output dir supplied"
  	echo "Usage: ./framy -s [start_time] -t [end_time] [name] [directory] [video.mp4]"
  exit 1
fi

# check output name
if [ -z "$NAME" ]
  then
    echo "Error. No output name supplied"
  	echo "Usage: ./framy -s [start_time] -t [end_time] [name] [directory] [video.mp4]"
  exit 1
fi


# create gif directory
if [ ! -d "$DIR" ]; then
	mkdir $DIR 
fi

# extract gif
./gifmy.sh -s $START -t $END $INPUT $DIR/$NAME.gif

# create frames directory
if [ ! -d "$DIR/$NAME" ]; then
	mkdir $DIR/$NAME 
fi


# extract frames
./extract_gif.sh $DIR/$NAME.gif $DIR/$NAME 
