#!/bin/sh

if [ -z "$1" ]
  then
    echo  "Error. input video"
  	echo  "Usage: ./to_mp4 <input_video.avi> <output_video.mp4>"
  exit 1
fi

ffmpeg -i $1 -c:v libx264 -preset slow -crf 19 -c:a libvo_aacenc -b:a 128k $2