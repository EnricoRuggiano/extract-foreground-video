#!/bin/sh

# check input segmented streaming
if [ -z "$1" ]
  then
    echo "Error. No input gif supplied"
  	echo "Usage: ./extract_gif [mygif.gif] [directory]"
  exit 1
fi

# check output file
if [ -z "$2" ]
  then
    echo "Error. No output directory supplied"
  	echo "Usage: ./extract_gif [mygif.gif] [directory]"
  exit 1
fi
ffmpeg -i $1 -vsync 0 $2/frame%d.png